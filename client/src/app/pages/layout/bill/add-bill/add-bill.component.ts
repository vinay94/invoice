import { Component, OnInit } from '@angular/core';
import { BillHeaderVo } from 'src/app/vo/bill-header.vo';
import { BillDetailVo } from 'src/app/vo/bill-detail.vo';
import { BillDto } from 'src/app/dto/bill.dto';
import { CustomerService } from 'src/app/service/customer.service';
import { ProductService } from 'src/app/service/product.service.';
import { ApiResponse } from 'src/app/dto/api-response';
import { CustomerVo } from 'src/app/vo/customer.vo';
import { ProductVo } from 'src/app/vo/product.vo.';
import { BillService } from 'src/app/service/bill.service';

@Component({
  selector: 'add-bill',
  templateUrl: './add-bill.component.html',
  styleUrls: ['./add-bill.component.scss']
})
export class AddBillComponent implements OnInit {

  /* ************************************ Static Fields ************************************ */
  customers: Array<CustomerVo>;
  products: Array<ProductVo>;

  /* ************************************ Instance Fields ********************************** */
  billDto: BillDto;
  billDetailArray: Array<BillDetailVo>
  billDetailVo: BillDetailVo = <BillDetailVo>{};
  showInitRow: boolean = true;

  /* ************************************ Constructors ************************************ */
  constructor(
    private customerApi: CustomerService,
    private productApi: ProductService,
    private billApi: BillService
  ) { }

  /* ************************************ Public Methods ********************************** */
  ngOnInit() {
    this._reset();
    this._getCustomers();
    this._getProducts();
  }

  public addRow(): void {
    this.billDto.details.push(this.billDetailVo)
    this.billDetailVo = <BillDetailVo>{};
  }

  addBill() {
    this.billDto.details.push(this.billDetailVo);
    this.showInitRow = false;
    this.billApi.addUpdateBill(this.billDto).subscribe((res: ApiResponse<any>) => {
      if (res.success) {
        this._reset();
      }
    })
    console.log('xxxxxxxx xxxxxxxxx ', this.billDto);
  }

  deleteRow(index) {
    this.billDto.details.splice(index, 1);
  }


  /* ********************************** Private Methods ************************************ */
  private _reset(): void {
    this.products = <Array<ProductVo>>[];
    this.customers = <Array<CustomerVo>>[];
    this.billDto = <BillDto>{};
    this.billDto.details = <Array<BillDetailVo>>[];
    this.billDto.header = <BillHeaderVo>{};
    this.billDto.header = <BillHeaderVo>{};
  }

  private _getCustomers() {
    this.customerApi.getAllCustomers().subscribe((res: ApiResponse<Array<CustomerVo>>) => {
      console.log('xxxxxx xxxxxxx xxxxxx customers ', res);
      if (res.success) {
        this.customers = res.body;
      }
    });
  }

  private _getProducts() {
    this.productApi.getAllProducts().subscribe((res: ApiResponse<Array<ProductVo>>) => {
      console.log('xxxxxx xxxxxxx xxxxxx ProductVo ', res);
      if (res.success) {
        this.products = res.body;
      }
    });
  }
}
