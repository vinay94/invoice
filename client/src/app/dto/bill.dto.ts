import { BillHeaderVo } from "../vo/bill-header.vo";
import { BillDetailVo } from "../vo/bill-detail.vo";

export interface BillDto {
    header: BillHeaderVo;
    details: Array<BillDetailVo>;
}