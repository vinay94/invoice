import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bill',
  templateUrl: './bill.component.html',
  styleUrls: ['./bill.component.scss']
})
export class BillComponent implements OnInit {

  /* ************************************ Static Fields ************************************ */

  /* ********************************** Instance Fields ************************************ */
  showAddBill: boolean = false;

  /* ************************************ Constructors ************************************ */
  constructor() { }

  /* ************************************ Public Methods ********************************** */
  ngOnInit() {
  }

  public enableAddBill(): void {
    this.showAddBill = true;
  }

  /* ********************************** Private Methods ************************************ */




}
