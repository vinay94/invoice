module.exports = (sequelize, DataTypes) => {
    let customer = sequelize.define('customer', {
        cid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,

        },
        ccode: {
            type: DataTypes.STRING,
        },

        cname: {
            type: DataTypes.STRING
        }

    }, {
            timestamps: false,
        });
    return customer;
};