import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginVo } from '../vo/login.vo';
import { Observable } from 'rxjs';
import { ApiResponse } from '../dto/api-response';
import { URL } from '../const/url';
import { KeyValueStorageService } from './key-value-storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';

const helper = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})

export class AuthApi {

  /* ************************************ Constructors ************************************ */
  constructor(
    private http: HttpClient,
    private storage: KeyValueStorageService
  ) { }

  /* ************************************ Public Methods ************************************ */
  public login(loginVo: LoginVo): Observable<ApiResponse<string>> {
    return this.http.post<ApiResponse<string>>(URL.LOGIN, loginVo);
  }

  public isAuthenticated(): boolean {
    const token = this.storage.getToken();

    if (token) {
      return true;
    }
    return false;
  }

  public saveToken(token: string) {
    this.storage.saveToken(token);
  }

  public getUser() {

  }
}
