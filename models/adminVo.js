module.exports = (sequelize, DataTypes) => {
    let admin = sequelize.define('admin', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,

        },
        cell: {
            type: DataTypes.BIGINT,
        },

        password: {
            type: DataTypes.STRING
        }

    }, {
        timestamps: false,
    });

    return admin;
};