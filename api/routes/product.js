const express = require('express');
const router = express.Router();
const conProduct = require('../controller/product-con');
const checkAuth = require('../middleware/check-auth');


/* ****************************Add Event PostGrase**************************** */
router.post('/add', checkAuth, conProduct.addProduct);

// /* ****************************Get Event By Id**************************** */
router.get('/get', conProduct.getAllProducts);




module.exports = router;