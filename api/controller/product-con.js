const db = require('../../models');
const response = require('../common/response');

exports.addProduct = (req, res, next) => {
    db.product.create({
        pname: req.body.pname,
        pcode: req.body.pcode
    }).then((result) => {
        const resObj = response.getResponseObj(true, "product Added Successfully", result);
        res.status(201).json(resObj);
    }).catch(err => {
        const resObj = response.getResponseObj(false, "Backend Error", err);
        res.status(201).json(resObj);
    });
}

exports.getAllProducts = (req, res, next) => {
    db.product.findAll({}).then((result) => {
        const resObj = response.getResponseObj(true, "All Products", result);
        res.status(201).json(resObj);
    }).catch(err => {
        const resObj = response.getResponseObj(false, "Backend Error", err);
        res.status(201).json(resObj);
    });
}