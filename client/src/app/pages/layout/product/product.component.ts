import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/service/product.service.';
import { ProductVo } from 'src/app/vo/product.vo.';
import { ApiResponse } from '../../../dto/api-response';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  /* ************************************ Static Fields ************************************ */
  productVo: ProductVo;

  /* ************************************ Instance Fields *********************************** */


  /* ************************************ Constructors ************************************ */
  constructor(private product: ProductService) { }

  /* ************************************ Public Methods ************************************ */
  ngOnInit() {
    this._reset();
  }

  public addProduct() {
    console.log('xxxxxxx xxxxxxxxxx xxxxxxx customer ', this.productVo);
    this.product.addUpdateProduct(this.productVo).subscribe((apiResponse: ApiResponse<any>) => {
      if (apiResponse.success) {
        this._reset();
      }
      console.log('xxxxxxxxx xxxxxxxxxxx xxxxxxxxx res is ', apiResponse);

    });
  }

  /* ************************************ Private Methods ********************************** */
  private _reset() {
    this.productVo = <ProductVo>{};
  }








}
