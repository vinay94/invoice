import { LoginVo } from './../../vo/login.vo';
import { Component, OnInit } from '@angular/core';
import { UserVo } from '../../vo/user.vo';
import { RegisterService } from '../../service/register.service';
import { ApiResponse } from '../../dto/api-response';
import { AuthApi } from '../../service/auth.api';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  /* ************************************ Static Fields ************************************ */

  /* ************************************ Instance Fields ************************************ */
  //model
  loginVo: LoginVo;
  usrVo: UserVo;
  //UI
  change: boolean = true;

  /* ************************************ Constructors ************************************ */
  constructor(
    private regService: RegisterService,
    private authApi: AuthApi,
    public router: Router
  ) { }

  /* ************************************ Public Methods ************************************ */
  ngOnInit() {
    this._init();
  }

  public login() {
    this.authApi.login(this.loginVo).subscribe((apiResponse: ApiResponse<string>) => {
      console.log('xxxxxxx xxxxxxxx xxxxxxxx res is ', apiResponse);
      if (apiResponse.success) {
        this.authApi.saveToken(apiResponse.body);
        this.router.navigate(['/']);
      }
    });
    console.log('xxxx xxxxxxxxx xxxxxxxxx login ', this.loginVo);
  }

  /* ********************************** Private Methods ************************************ */
  private _init() {
    this._reset();
  }

  private _reset() {
    this.loginVo = <LoginVo>{};
    this.usrVo = <UserVo>{};
  }

}
