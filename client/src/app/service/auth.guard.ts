import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthApi } from './auth.api';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  /* ************************************ Constructors ************************************ */
  constructor(public auth: AuthApi, public router: Router) { }

  /* ************************************ Public Methods ************************************ */
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }
    return true;
  }
}
