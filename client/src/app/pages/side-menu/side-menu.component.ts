import { Component, OnInit } from '@angular/core';
import { MenuVo } from '../../vo/menu.vo';

@Component({
  selector: 'eg-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})

export class SideMenuComponent implements OnInit {

  /* ************************************ Static Fields ************************************ */

  /* ************************************ Instance Fields ************************************ */
  public menuVos: Array<MenuVo>;

  /* ************************************ Constructors ************************************ */
  constructor() { }


  /* ************************************ Public Methods ************************************ */
  ngOnInit() {
    this._init();
  }

  /* ************************************ Private Methods ************************************ */
  private _init() {
    this.menuVos = <Array<MenuVo>>[];
  }

  private _setLink() {
  }



}
