import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URL } from '../const/url';
import { ApiResponse } from '../dto/api-response';
import { ProductVo } from '../vo/product.vo.';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  /* ************************************ Constructors ************************************ */
  constructor(private http: HttpClient) { }

  /* ************************************ Public Methods ************************************ */
  getAllProducts(): Observable<ApiResponse<Array<ProductVo>>> {
    return this.http.get<ApiResponse<Array<ProductVo>>>(URL.GET_PRODUCTS);
  }
  addUpdateProduct(productVo: ProductVo): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(URL.ADD_PRODUCT, productVo);
  }
}
