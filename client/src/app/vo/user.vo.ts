export interface UserVo {
    _id: string;
    name: string;
    email: string;
    role: string;
    cell: number;
    pass: string;
}