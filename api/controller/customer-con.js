const db = require('../../models');
const response = require('../common/response');

exports.addCustomer = (req, res, next) => {
    db.customer.create({
        cname: req.body.cname,
        ccode: req.body.ccode
    }).then((result) => {
        const resObj = response.getResponseObj(true, "All Customer Added Successfully", result);
        res.status(201).json(resObj);
    }).catch(err => {
        const resObj = response.getResponseObj(false, "Backend Error", err);
        console.log('xxxxxxxxxxxx xxxxxxxxxxxxx err ', err)
        res.status(401).json(resObj);
    });
}

exports.getAllCustomers = (req, res, next) => {
    db.customer.findAll({}).then((result) => {
        const resObj = response.getResponseObj(true, "All Customers", result);
        res.status(201).json(resObj);
    }).catch(err => {
        const resObj = response.getResponseObj(false, "Backend Error", err);
        res.status(201).json(resObj);
    });
}

