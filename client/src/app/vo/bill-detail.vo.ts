export interface BillDetailVo {
    inv_d_id: number;
    quantity: number;
    rate: number;
    amount: number;
    pdt_id: number; // Product id
}