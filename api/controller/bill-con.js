const db = require('../../models');
const response = require('../common/response');

exports.addBill = (req, res, next) => {
    db.invoice_header.create({
        invoiceDate: req.body.header.invoiceDate,
        cus_id: req.body.header.cus_id
    }).then((result) => {
        console.log('xxxx xxxxxxxx xxxxxxx result is ', result);
        if (result.invoiceNo) {
            _addInvoiceDetails(req.body.details, result.invoiceNo).then(resp => {
                console.log('xxxxxxx xxxxxxxxx xxxxxxxx resp ', resp)
                if (resp) {
                    const resObj = response.getResponseObj(true, "bill details Added Successfully", resp);
                    res.status(201).json(resObj);
                }
            }).catch(err => {
                const resObj = response.getResponseObj(false, "Backend Error", err);
                res.status(201).json(resObj);
            });
        }
        // const resObj = response.getResponseObj(true, "bill Added Successfully", result);
        // res.status(201).json(resObj);
    }).catch(err => {
        console.log('xxxx xxxxxxxx xxxxxxx err is ', err);
        const resObj = response.getResponseObj(false, "Backend Error", err);
        res.status(201).json(resObj);
    });
}

exports.getBill = (req, res, next) => {
    db.invoice_detail.findAll({
        model: db.invoice_header,
        where: ["inv_no = invoiceNo"]
    }).then((result) => {
        const resObj = response.getResponseObj(true, "All Bills", result);
        res.status(201).json(resObj);
    }).catch(err => {
        console.log('xxxxxxxxxx xxxxxxxxxx xxxxxxxxxxxxx ', err)
        const resObj = response.getResponseObj(false, "Backend Error", err);
        res.status(201).json(resObj);
    });

}

// exports.getAllProducts = (req, res, next) => {
//     db.product.findAll({}).then((result) => {
//         const resObj = response.getResponseObj(true, "All Products", result);
//         res.status(201).json(resObj);
//     }).catch(err => {
//         const resObj = response.getResponseObj(false, "Backend Error", err);
//         res.status(201).json(resObj);
//     });
// }

function _addInvoiceDetails(details, invoiceNo) {
    return new Promise((resolve, reject) => {
        let arr = [];
        details.forEach(val => {
            arr.push({
                ...val,
                inv_no: invoiceNo
            })
        });
        console.log('xxxxxxxxx xxxxxxx xres from ', ...arr);

        db.invoice_detail.bulkCreate(arr).then(res => {
            console.log('xxxxxxxxx xxxxxxx xres from ', res);
            resolve(res);
        }).catch(err => {
            console.log('xxxxxxxxx xxxxxxx xres from ', err);
            reject(err);
        })
    });
}