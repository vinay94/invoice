const express = require('express');
const router = express.Router();
const conCustomer = require('../controller/customer-con');
const checkAuth = require('../middleware/check-auth');


/* ****************************Add Event PostGrase**************************** */
router.post('/add', checkAuth, conCustomer.addCustomer);

/* ****************************Get Event By Id**************************** */
router.get('/get', conCustomer.getAllCustomers);




module.exports = router;