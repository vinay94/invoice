const http = require('http');
const app = require('./app');
const port = process.env.PORT || 3300;
const db = require('./models')

const server = http.createServer(app);
db.sequelize.sync().then( () => {
    server.listen(port, () => {
        console.log(`Listening on PORT ${port}`);
    });
});