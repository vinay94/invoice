module.exports = (sequelize, DataTypes) => {
    let invoice_detail = sequelize.define('invoice_detail', {
        inv_d_id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },

        quantity: {
            type: DataTypes.INTEGER,
        },

        rate: {
            type: DataTypes.INTEGER,
        },

        amount: {
            type: DataTypes.INTEGER,
        },

        pdt_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },

        inv_no: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }
    }, {
            timestamps: false,
        });
    return invoice_detail;
};