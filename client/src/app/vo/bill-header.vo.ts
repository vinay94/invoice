export interface BillHeaderVo {
    invoiceNo: number;
    invoiceDate: Date;
    cus_id: number;
}