module.exports = (sequelize, DataTypes) => {
    let invoice_header = sequelize.define('invoice_header', {
        invoiceNo: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        
        invoiceDate: {
            type: DataTypes.DATE,
        },

        cus_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        }

    }, {
            timestamps: false,
        });
        
        

    return invoice_header;
};