const BASE = 'http://localhost:3300';

export const URL = {
    LOGIN: BASE + '/api/auth/admin',
    ADD_CUSTOMER: BASE + '/api/customer/add',
    GET_CUSTOMERS: BASE + '/api/customer/get',
    ADD_PRODUCT: BASE + '/api/product/add',
    GET_PRODUCTS: BASE + '/api/product/get',
    ADD_BILL: BASE + '/api/bill/add',


    // GET_CUSTOMER: BASE + '/egarage/api/customer/get'
}