import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URL } from '../const/url';
import { ApiResponse } from '../dto/api-response';
import { CustomerVo } from '../vo/customer.vo';


@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  /* ************************************ Constructors ************************************ */
  constructor(private http: HttpClient) { }

  /* ************************************ Public Methods ************************************ */
  getAllCustomers(): Observable<ApiResponse<Array<CustomerVo>>> {
    return this.http.get<ApiResponse<Array<CustomerVo>>>(URL.GET_CUSTOMERS);
  }

  addUpdateCustomer(customerVo: CustomerVo): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(URL.ADD_CUSTOMER, customerVo);
  }
}
