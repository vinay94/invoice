import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { URL } from '../const/url';
import { ApiResponse } from '../dto/api-response';
import { BillDto } from '../dto/bill.dto';


@Injectable({
  providedIn: 'root'
})
export class BillService {

  /* ************************************ Constructors ************************************ */
  constructor(private http: HttpClient) { }

  /* ************************************ Public Methods ************************************ */
//   getAllCustomers(): Observable<ApiResponse<Array<CustomerVo>>> {
//     return this.http.get<ApiResponse<Array<CustomerVo>>>(URL.GET_CUSTOMERS);
//   }

  addUpdateBill(billDto: BillDto): Observable<ApiResponse<any>> {
    return this.http.post<ApiResponse<any>>(URL.ADD_BILL, billDto);
  }
}
