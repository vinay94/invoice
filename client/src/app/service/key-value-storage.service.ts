import { Injectable } from '@angular/core';
import {LocalStorageService} from 'ngx-webstorage';
import { DbKey } from '../enum/db-key.enum';

@Injectable({
  providedIn: 'root'
})
export class KeyValueStorageService {

  /* ************************************ Constructors ************************************ */
  constructor(private storage: LocalStorageService) { }

  /* ************************************ Public Methods ************************************ */
  public saveToken(token: string) {
    const authToken = this._toString(token);
    this.storage.store(DbKey[DbKey.AUTH_TOKEN], authToken);
  }

  public getToken() {
    const authToken = this.storage.retrieve(DbKey[DbKey.AUTH_TOKEN]);
    if(authToken) {
      return this._toJson(authToken);
    }
  }

  /* ************************************ Private Methods ************************************ */
  private _toJson(token: string) {
    return JSON.parse(token);
  }

  private _toString(token: string) {
    return JSON.stringify(token);
  }

}
