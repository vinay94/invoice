import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../service/customer.service';
import { ApiResponse } from '../../../dto/api-response';
import { CustomerVo } from '../../../vo/customer.vo';


@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  /* ************************************ Static Fields ************************************ */
  customerVo: CustomerVo;

  /* ************************************ Instance Fields *********************************** */


  /* ************************************ Constructors ************************************ */
  constructor(private customer: CustomerService) { }

  /* ************************************ Public Methods ************************************ */
  ngOnInit() {
    this._reset();
  }

  public addCustomer() {
    console.log('xxxxxxx xxxxxxxxxx xxxxxxx customer ', this.customerVo);
    this.customer.addUpdateCustomer(this.customerVo).subscribe((apiResponse: ApiResponse<any>) => {
      if (apiResponse.success) {
        this._reset();
      }
      console.log('xxxxxxxxx xxxxxxxxxxx xxxxxxxxx res is ', apiResponse);

    });
  }

  /* ************************************ Private Methods ********************************** */
  private _reset() {
    this.customerVo = <CustomerVo>{};
  }








}
