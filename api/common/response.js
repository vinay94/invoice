exports.getResponseObj = (success, message, body = {}) => {
    const res = {
        "success": success,
        "msg": message,
        "body": body
    }

    return res;
}