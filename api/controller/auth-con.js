const db = require('../../models');
const response = require('../common/response')
const jwt = require('jsonwebtoken');

exports.auth = (req, res, next) => {
    db.admin.findOne({
        where: {
            cell: req.body.cell,
            password: req.body.pass
        }
    }).then(result => {
        let resp = {}
        if (result) {
            resp = response.getResponseObj(true, "Loggedin Successfully", getToken(result))
        } else {
            resp = response.getResponseObj(false, "unathorised");
        }
        res.status(201).json(resp);
    }).catch({
        
    });
}

function getToken(result) {
    const token = jwt.sign({
        cell: result.cell,
        id: result.id,
        },
        'JWT_TOKEN_SECRET',
        {
            expiresIn: "5h"
        });

        return token;
}