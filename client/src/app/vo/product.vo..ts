export interface ProductVo {
    pid: number;
    pcode: string;
    pname: string;
}