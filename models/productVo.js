module.exports = (sequelize, DataTypes) => {
    let product = sequelize.define('product', {
        pid: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,

        },
        pcode: {
            type: DataTypes.STRING,
        },

        pname: {
            type: DataTypes.STRING
        }

    }, {
            timestamps: false,
        });

    return product;
};