export interface CustomerVo {
    cid: number;
    ccode: string;
    cname: string;
}