import { LayoutComponent } from './layout.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { LayoutRoutingModule } from './layout-routing.module';
import { TestComponent } from './test/test.component';
import {
  MatSidenavModule,
  MatIconModule,
  MatToolbarModule,
  MatListModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule
} from '@angular/material';
import { SideMenuComponent } from '../side-menu/side-menu.component';
import { CustomerComponent } from './customer/customer.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ProductComponent } from './product/product.component';
import { BillComponent } from './bill/bill.component';
import { AddBillComponent } from './bill/add-bill/add-bill.component';

const MATERIAL = [
  MatSidenavModule,
  MatIconModule,
  MatToolbarModule,
  MatListModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatSelectModule
];

const COMPONENTS = [
LayoutComponent, TestComponent, SideMenuComponent, CustomerComponent, ProductComponent,BillComponent, AddBillComponent   
]

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ...MATERIAL,
    FlexLayoutModule
  ],
  declarations: [...COMPONENTS],
  providers: [  
    MatDatepickerModule,  
  ],
})
export class LayoutModule { }
