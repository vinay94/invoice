import { Injectable } from '@angular/core';
import { URL } from '../const/url'
import { UserVo } from '../vo/user.vo';
import { ApiResponse } from '../dto/api-response';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  /* ************************************ Constructors ************************************ */
  constructor(private http: HttpClient) { }

  /* ************************************ Public Methods ************************************ */
  // addUpdateUser(usrVo: UserVo): Observable<ApiResponse<any>> {
  //   return this.http.post<ApiResponse<any>>(URL.REGISTER, usrVo);
  // }
}
