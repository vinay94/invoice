import { Injectable } from '@angular/core';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { KeyValueStorageService } from './key-value-storage.service';


@Injectable()

export class AuthHttpInterceptor implements HttpInterceptor {

    constructor(private storage: KeyValueStorageService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        request = request.clone({
            setHeaders: {
                Authorization: `Bearer ${this.storage.getToken()}`
            }
        });
        return next.handle(request);
    }
}
