const express = require('express');
const router = express.Router();
const conBill = require('../controller/bill-con');
const checkAuth = require('../middleware/check-auth');


/* ****************************Add Event PostGrase**************************** */
router.post('/add', conBill.addBill);

// /* ****************************Get Event By Id**************************** */
router.get('/get', conBill.getBill);




module.exports = router;